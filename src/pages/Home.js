import React from "react";
import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
import FeaturedProducts from "../components/FeaturedProducts";
import { Container, Row, Col } from "react-bootstrap";

export default function Home() {
  const bannerData = {
    title: "Welcome to Clover! 🍀",
    content: "From our cozy corner of the world, to yours.",
    destination: "/products",
    label: "Shop Now",
  };

  return (
    <>
      <Container className="my-5 py-2">
        <Row className="d-md-flex d-none justify-content-center align-items-center">
          {/* Logo column */}
          <Col md={4}>
            <img src="/logo.png" alt="Logo" className="storeLogo" />
          </Col>
          {/* Banner column */}
          <Col md={7}>
            <Banner data={bannerData} />
          </Col>
        </Row>

        {/* Row for small screens (sm and below) */}
        <Row className="d-md-none align-items-center">
          {/* Logo and banner stacked on small screens */}
          <Col sm={12}>
            <img src="/logo.png" alt="Logo" className="storeLogo" />
          </Col>
          <Col sm={12}>
            <Banner data={bannerData} />
          </Col>
        </Row>

        {/* Highlights & Store Description */}
        <Row>
          <Highlights />
        </Row>
        {/* Featured products */}
        <Row className="mt-4">
          <Col>
            <FeaturedProducts />
          </Col>
        </Row>
      </Container>
    </>
  );
}
