import { Container, Row, Col } from "react-bootstrap";

function Footer() {
  const currentYear = new Date().getFullYear();
  return (
    <footer className="footer">
      <Container fluid>
        <Row className="text-center">
          <Col>
            <h4>Clover Bunbougu-ya</h4>
            <span role="img" aria-label="Clover">
              🍀
            </span>
            <p>
              Bringing cozy cuteness to your home. &copy; {currentYear} MDLS.
              All rights reserved.
            </p>
          </Col>
        </Row>
      </Container>
    </footer>
  );
}

export default Footer;
